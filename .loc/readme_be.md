# Lacinizatar-rs

[English version](../readme.md) 

## Што гэта такое
Гэта бібліятэка для пераўтварэння радка на кірыліцы ў радок на лацінцы праз просты макрас у Rust.

## Навошта створана гэтая бібліятэка
Бо такой бібліятэкі для раста няма. Таксама маю намер выкарыстоўваць гэтую бібліятэку ў іншых маіх праектах.
Такіх як:
- [Kastler](https://gitlab.com/nafaivel_189/kastler) 

## Выкарыстанне
Cargo.toml
``` toml
lacinizatar = { git = "https://gitlab.com/Nafaivel/lacinizatar-rs"}
```
rust
``` rust
use lacinizatar::lacinizatar;

// макрас атрымлівае String і перадае &String пры выкліку функцыі lacinize_string
lacinize!(string)
// перадайце true, каб уключыць lĺ замест łl
lacinize!(string, true)
```
