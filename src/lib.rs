use unicode_segmentation::UnicodeSegmentation;

/// input can be:
/// ("string to lacineze")
/// ("string to lacineze", true)
/// set true for lĺ instead of łl
#[macro_export]
macro_rules! lacinize {
    ($s:expr) => {
        $crate::lacinize_string(&$s, false)
    };
    ($s:expr, $t:expr) => {
        $crate::lacinize_string(&$s, $t)
    };
}
// TODO: add caps support
#[doc(hidden)]
pub fn lacinize_string(string: &str, typ: bool) -> String {
    let mut flags = Flags::new(typ);
    let mut lacinizavanaja_straka = String::new();
    for letter in string.graphemes(true) {
        let to_push: &str = match letter {
            // Transformed vowel
            "Я" => {
                let r = if flags.l_before_and_not_typ() {
                    l_to_l(&mut lacinizavanaja_straka, true, typ);
                    "A"
                } else if flags.soft_vowel_begining_or_after_other_vovel() {
                    "Ja"
                } else {
                    fix_softness_through_one(&mut lacinizavanaja_straka, typ);
                    "Ia"
                };
                flags.vowel();
                r
            }
            "я" => {
                let r = if flags.l_before_and_not_typ() {
                    l_to_l(&mut lacinizavanaja_straka, true, typ);
                    "a"
                } else if flags.soft_vowel_begining_or_after_other_vovel() {
                    "ja"
                } else {
                    fix_softness_through_one(&mut lacinizavanaja_straka, typ);
                    "ia"
                };
                flags.vowel();
                r
            }
            "Ё" => {
                let r = if flags.l_before_and_not_typ() {
                    l_to_l(&mut lacinizavanaja_straka, true, typ);
                    "O"
                } else if flags.soft_vowel_begining_or_after_other_vovel() {
                    "Jo"
                } else {
                    fix_softness_through_one(&mut lacinizavanaja_straka, typ);
                    "Io"
                };
                flags.vowel();
                r
            }
            "ё" => {
                let r = if flags.l_before_and_not_typ() {
                    l_to_l(&mut lacinizavanaja_straka, true, typ);
                    "o"
                } else if flags.soft_vowel_begining_or_after_other_vovel() {
                    "jo"
                } else {
                    fix_softness_through_one(&mut lacinizavanaja_straka, typ);
                    "io"
                };
                flags.vowel();
                r
            }
            "Е" => {
                let r = if flags.l_before_and_not_typ() {
                    l_to_l(&mut lacinizavanaja_straka, true, typ);
                    "E"
                } else if flags.soft_vowel_begining_or_after_other_vovel() {
                    "Je"
                } else {
                    fix_softness_through_one(&mut lacinizavanaja_straka, typ);
                    "Ie"
                };
                flags.vowel();
                r
            }
            "е" => {
                let r = if flags.l_before_and_not_typ() {
                    l_to_l(&mut lacinizavanaja_straka, true, typ);
                    "e"
                } else if flags.soft_vowel_begining_or_after_other_vovel() {
                    "je"
                } else {
                    fix_softness_through_one(&mut lacinizavanaja_straka, typ);
                    "ie"
                };
                flags.vowel();
                r
            }
            "Ю" => {
                let r = if flags.l_before_and_not_typ() {
                    l_to_l(&mut lacinizavanaja_straka, true, typ);
                    "U"
                } else if flags.soft_vowel_begining_or_after_other_vovel() {
                    "Ju"
                } else {
                    fix_softness_through_one(&mut lacinizavanaja_straka, typ);
                    "Iu"
                };
                flags.vowel();
                r
            }
            "ю" => {
                let r = if flags.l_before_and_not_typ() {
                    l_to_l(&mut lacinizavanaja_straka, true, typ);
                    "u"
                } else if flags.soft_vowel_begining_or_after_other_vovel() {
                    "ju"
                } else {
                    fix_softness_through_one(&mut lacinizavanaja_straka, typ);
                    "iu"
                };
                flags.vowel();
                r
            }

            // Not transformed vowel
            "І" | "И" => {
                if flags.l_before {
                    l_to_l(&mut lacinizavanaja_straka, true, typ);
                }
                fix_softness_through_one(&mut lacinizavanaja_straka, typ);
                flags.vowel();
                "I"
            }
            "і" | "и" => {
                if flags.l_before {
                    l_to_l(&mut lacinizavanaja_straka, true, typ);
                }
                fix_softness_through_one(&mut lacinizavanaja_straka, typ);
                flags.vowel();
                "i"
            }
            "А" => {
                if flags.l_before {
                    l_to_l(&mut lacinizavanaja_straka, false, typ);
                }
                flags.vowel();
                "A"
            }
            "а" => {
                if flags.l_before {
                    l_to_l(&mut lacinizavanaja_straka, false, typ);
                }
                flags.vowel();
                "a"
            }
            "О" => {
                if flags.l_before {
                    l_to_l(&mut lacinizavanaja_straka, false, typ);
                }
                flags.vowel();
                "O"
            }
            "о" => {
                if flags.l_before {
                    l_to_l(&mut lacinizavanaja_straka, false, typ);
                }
                flags.vowel();
                "o"
            }
            "У" => {
                if flags.l_before {
                    l_to_l(&mut lacinizavanaja_straka, false, typ);
                }
                flags.vowel();
                "U"
            }
            "у" => {
                if flags.l_before {
                    l_to_l(&mut lacinizavanaja_straka, false, typ);
                }
                flags.vowel();
                "u"
            }
            "Ы" => {
                if flags.l_before {
                    l_to_l(&mut lacinizavanaja_straka, false, typ);
                }
                flags.vowel();
                "Y"
            }
            "ы" => {
                if flags.l_before {
                    l_to_l(&mut lacinizavanaja_straka, false, typ);
                }
                flags.vowel();
                "y"
            }
            "Э" => {
                if flags.l_before {
                    l_to_l(&mut lacinizavanaja_straka, false, typ);
                }
                flags.vowel();
                "E"
            }
            "э" => {
                if flags.l_before {
                    l_to_l(&mut lacinizavanaja_straka, false, typ);
                }
                flags.vowel();
                "e"
            }

            // signs
            "\'" => {
                flags.after_vowel = true;
                flags.l_before = false;
                flags.word_beginnging = false;
                ""
            }
            " " => {
                flags.space();
                " "
            }
            "Ь" => {
                if let Some(c) = lacinizavanaja_straka.pop() {
                    lacinizavanaja_straka.push_str(&make_soft(&c.to_string(), typ))
                }
                ""
            }
            "ь" => {
                if let Some(c) = lacinizavanaja_straka.pop() {
                    lacinizavanaja_straka.push_str(&make_soft(&c.to_string(), typ))
                }
                ""
            }

            // consonants
            "Л" => {
                flags.l();
                "L"
            }
            "л" => {
                flags.l();
                "l"
            }
            "Ў" => {
                flags.consonant();
                "Ŭ"
            }
            "ў" => {
                flags.consonant();
                "ŭ"
            }
            "Б" => {
                flags.consonant();
                "B"
            }
            "В" => {
                flags.consonant();
                "V"
            }
            "Г" => {
                flags.consonant();
                "H"
            }
            "Д" => {
                flags.consonant();
                "D"
            }
            "Ж" => {
                flags.consonant();
                "Ž"
            }
            "З" => {
                flags.consonant();
                "Z"
            }
            "Й" => {
                flags.consonant();
                "J"
            }
            "К" => {
                flags.consonant();
                "K"
            }
            "М" => {
                flags.consonant();
                "M"
            }
            "Н" => {
                flags.consonant();
                "N"
            }
            "П" => {
                flags.consonant();
                "P"
            }
            "Р" => {
                flags.consonant();
                "R"
            }
            "С" => {
                flags.consonant();
                "S"
            }
            "Т" => {
                flags.consonant();
                "T"
            }
            "Ф" => {
                flags.consonant();
                "F"
            }
            "Х" => {
                flags.consonant();
                "Ch"
            }
            "Ц" => {
                flags.consonant();
                "C"
            }
            "Ч" => {
                flags.consonant();
                "Č"
            }
            "Ш" => {
                flags.consonant();
                "Š"
            }
            "б" => {
                flags.consonant();
                "b"
            }
            "в" => {
                flags.consonant();
                "v"
            }
            "г" => {
                flags.consonant();
                "h"
            }
            "д" => {
                flags.consonant();
                "d"
            }
            "ж" => {
                flags.consonant();
                "ž"
            }
            "з" => {
                flags.consonant();
                "z"
            }
            "й" => {
                flags.consonant();
                "j"
            }
            "к" => {
                flags.consonant();
                "k"
            }
            "м" => {
                flags.consonant();
                "m"
            }
            "н" => {
                flags.consonant();
                "n"
            }
            "п" => {
                flags.consonant();
                "p"
            }
            "р" => {
                flags.consonant();
                "r"
            }
            "с" => {
                flags.consonant();
                "s"
            }
            "т" => {
                flags.consonant();
                "t"
            }
            "ф" => {
                flags.consonant();
                "f"
            }
            "х" => {
                flags.consonant();
                "ch"
            }
            "ц" => {
                flags.consonant();
                "c"
            }
            "ч" => {
                flags.consonant();
                "č"
            }
            "ш" => {
                flags.consonant();
                "š"
            }
            _ => letter,
        };
        lacinizavanaja_straka.push_str(to_push);
    }
    lacinizavanaja_straka
}

fn l_to_l(string: &mut String, is_soft: bool, typ: bool) {
    if let Some(l) = string.pop() {
        match l {
            'l' => {
                if is_soft & !typ {
                    string.push('l')
                } else if !is_soft & !typ {
                    string.push('ł')
                    // } else if is_soft & typ {
                    //     string.push('ĺ')
                    // } else if !is_soft & typ {
                    //     string.push('l')
                }
            }
            'L' => {
                if is_soft & !typ {
                    string.push('L')
                } else if !is_soft & !typ {
                    string.push('Ł')
                    // } else if is_soft & typ {
                    //     string.push('Ĺ')
                    // } else if !is_soft & typ {
                    //     string.push('L')
                }
            }
            _ => {}
        }
    }
}

fn fix_softness_through_one(string: &mut String, typ: bool) {
    if let Some(last_char) = string.pop() {
        if let Some(prelast_char) = string.pop() {
            string.push_str(&make_soft(&String::from(prelast_char), typ));
        }
        string.push(last_char);
    }
}

fn make_soft(chr: &str, typ: bool) -> String {
    match chr {
        "C" => String::from("Ć"),
        "D" => String::from("Dz"),
        "L" => {
            if typ {
                String::from("Ĺ")
            } else {
                String::from("L")
            }
        }
        "Ł" => String::from("L"),
        "N" => String::from("Ń"),
        "S" => String::from("Ś"),
        "T" => String::from("Ć"),
        "Z" => String::from("Ź"),
        "c" => String::from("ć"),
        "d" => String::from("dz"),
        "l" => {
            if typ {
                String::from("ĺ")
            } else {
                String::from("l")
            }
        }
        "ł" => String::from("l"),
        "n" => String::from("ń"),
        "s" => String::from("ś"),
        "t" => String::from("ć"),
        "z" => String::from("ź"),
        _ => String::from(chr),
    }
}

struct Flags {
    word_beginnging: bool,
    after_vowel: bool,
    l_before: bool,
    typ: bool,
}

impl Flags {
    fn new(typ: bool) -> Flags {
        Flags {
            word_beginnging: true,
            after_vowel: false,
            l_before: false,
            typ,
        }
    }
    fn soft_vowel_begining_or_after_other_vovel(&self) -> bool {
        self.word_beginnging | self.after_vowel
    }
    fn l_before_and_not_typ(&self) -> bool {
        self.l_before & !self.typ
    }
    fn vowel(&mut self) {
        self.after_vowel = true;
        self.l_before = false;
        self.word_beginnging = false;
    }
    fn l(&mut self) {
        self.after_vowel = false;
        self.l_before = true;
        self.word_beginnging = false;
    }
    fn space(&mut self) {
        self.after_vowel = false;
        self.l_before = false;
        self.word_beginnging = true;
    }
    fn consonant(&mut self) {
        self.after_vowel = false;
        self.l_before = false;
        self.word_beginnging = false;
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn lacinize_string_softness_test() {
        let input = "сьнег насеньне снег насенне".to_string();
        let output = lacinize!(&input);
        println!("{input}");
        println!("{output}");
        assert_eq!(output, "śnieh nasieńnie śnieh nasieńnie".to_string());
    }

    #[test]
    fn lacinize_string_l_test() {
        let input = "лёгіка лацінка Літак Латы".to_string();
        let output = lacinize!(&input);
        println!("{input}");
        println!("{output}");
        assert_eq!(output, "lohika łacinka Litak Łaty".to_string());
    }

    #[test]
    fn lacinize_string_complex_test() {
        let input = "тэст лацінізатару распачынаецца прама зараз... Некалькі слоў, яшчэ словы... Павінна хапіць, але вось яшчэ сьнег ці мо снег?".to_string();
        let output = lacinize!(&input);
        println!("{input}");
        println!("{output}");
        assert_eq!(output, "test łacinizataru raspačynajecca prama zaraz... Niekalki słoŭ, jašče słovy... Pavinna chapić, ale voś jašče śnieh ci mo śnieh?".to_string());
    }
}
