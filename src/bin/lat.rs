use lacinizatar::lacinize;
use std::env::args;
use std::io::stdin;

fn main() {
    let args: Vec<String> = args().skip(1).collect();
    let pipe = stdin();
    for line in pipe.lines() {
        println!("{}", lacinize!(line.unwrap_or("".to_string())))
    }
    if !args.is_empty() {
        let text = args.join(" ");
        println!("{}", lacinize!(text))
    }
}
