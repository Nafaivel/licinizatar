# Lacinizatar-rs

[Беларуская вэрсія](.loc/readme_be.md) 

## What it is
This is library to convert cirilica string to lacinka with simple macro in rust.

## Why this library created
Because there is no such library for rust. Also intention to use this library in my other projects.
Such as:
- [Kastler](https://gitlab.com/nafaivel_189/kastler) 

## Usage
Cargo.toml
``` toml
lacinizatar = { git = "https://gitlab.com/Nafaivel/lacinizatar-rs"}
```
rust
``` rust
use lacinizatar::lacinizatar;

// macro get's String and pass &String in function call
lacinize!(string)
// pass true to enable lĺ instead of łl
lacinize!(string, true)
```
